﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SlicUnoBooksApplication.Base;
using SlicUnoBooksApplication.PageFactory;
using System;
using System.Threading;


namespace SlicUnoBooksApplication
{
    class VendorDetails : BaseTest
    {
        public static object[] VendorDetailsData()
        {
            object[] main = Utilities.ExcelUtil.GetsheetInttoObject(@"D:\svsb365\selenium\copy\uno\SlicUnoBooksApplication\SlicUnoBooksApplication\Data\VendorDetailsData.xlsx", "VendorData");
            return main;
        }

        [Test, TestCaseSource("VendorDetailsData")]
        public void Login(string username,string password,string unit,string finyear,string vendorname,string nameasperpan,string address1,string areasearchdropdown,string addresssearch,string contactperson,string pansearch,string pannumber,string TDSoption,string vendoroption,string phonenumber,string banksearch,string bankbrachsearch,string bankaccountnumer,string vendorfavour)                                             
        {
            driver.Url = "http://192.169.5.43/slicuno4.5";    
            LoginPage login = new LoginPage(driver);
            VendorDetailsPage vendorpage = new VendorDetailsPage(driver);
            login.Switchtowindow();           
            login.Username(username);           
            login.Password(password);          
            login.unit(unit);          
            login.finyearclear();      
            login.finyear(finyear);
            //to enter captcha
            Thread.Sleep(20000);
            login.submit();            
            vendorpage.Windowmenu();           
            vendorpage.Vendorscreenopen();            
            driver.SwitchTo().Frame(driver.FindElement(By.Id("frm40110409")));
            //vendorname
            vendorpage.Vendorname(vendorname);
            //name as per pan
            vendorpage.Nameasperpan(nameasperpan);
            //address
            vendorpage.Addressbutton();            
            //vendordetails mainframe
            driver.SwitchTo().Frame(driver.FindElement(By.Id("openframe")));
            Thread.Sleep(2000);
            ////address
            vendorpage.Address1(address1);
            //pincode button
            vendorpage.Pincodebutton();
            IWebElement pincodeframe = driver.FindElement(By.XPath("//iframe[contains(@src,'../Treasuryincludes/AddressOpenHelp.aspx?ColumnName=FetchPinCodes&SectionName=Masters&Title=Bank')]"));
            driver.SwitchTo().Frame(pincodeframe);
            //drop dowm search
            vendorpage.Addressssearchdropdown(areasearchdropdown);        
            //search address
            vendorpage.Addressssearchtextbox(addresssearch);
            //address selection
            vendorpage.Addressselection();
            //address ok button
            driver.SwitchTo().DefaultContent();
            driver.SwitchTo().Frame(driver.FindElement(By.Id("frm40110409")));
            driver.SwitchTo().Frame(driver.FindElement(By.Id("openframe")));
            //Addressokbutton
            vendorpage.Addressokbutton();
            driver.SwitchTo().DefaultContent();
            driver.SwitchTo().Frame(driver.FindElement(By.Id("frm40110409")));
            //contact person
            vendorpage.Contactperson(contactperson);
            //res button
            vendorpage.resstatus();
            IWebElement Resstatusframe = driver.FindElement(By.XPath("//iframe[contains(@src,'../includes/GA_HelpGetNRecords.aspx?H_Query=select')]"));
            driver.SwitchTo().Frame(Resstatusframe);
            //res searchbox
            vendorpage.Resstatussearch(pansearch);
            vendorpage.Resstatusselection();
            driver.SwitchTo().DefaultContent();
            driver.SwitchTo().Frame(driver.FindElement(By.Id("frm40110409")));
            //pannumber
            vendorpage.pannumber(pannumber);
            //TDS
            vendorpage.TDSselection(TDSoption);
            //option for vendor cboAstOption         
            vendorpage.optionselection(vendoroption);
            //phone number
            vendorpage.Mobilenumber(phonenumber);
            //bankdetail grid open 
            vendorpage.Bankdetailsdropdown();
            //add button
            vendorpage.BankdetailsAdd();
            //bank name picklist
            Thread.Sleep(1000);           
            vendorpage.Banknamepicklist();
            IWebElement frame =driver.FindElement(By.XPath("//iframe[contains(@src,'../includes/OpenHelp.aspx?ColumnName=BankShrtName&SectionName=Masters&Title=Bank')]"));
            driver.SwitchTo().Frame(frame);
            //bank search text
            vendorpage.Bankserchtext(banksearch);
            //bank selection
            vendorpage.Banknameselection();
            driver.SwitchTo().DefaultContent();
            driver.SwitchTo().Frame(driver.FindElement(By.Id("frm40110409")));
            //bank branch
            vendorpage.Bankbranchpicklist();
             Thread.Sleep(1000);
            IWebElement Bankbranchframe = driver.FindElement(By.XPath("//iframe[contains(@src,'../includes/OpenHelp.aspx?ColumnName=BankBranchName&SectionName=Masters&Title=Bank')]"));
            driver.SwitchTo().Frame(Bankbranchframe);
            //branch search
            vendorpage.Bankbranchsearchtext(bankbrachsearch);
            vendorpage.Bankbranchselection();
            driver.SwitchTo().DefaultContent();
            driver.SwitchTo().Frame(driver.FindElement(By.Id("frm40110409")));
            //bank account number
            vendorpage.Bankaccountnumber(bankaccountnumer);
            //vendor favour
            vendorpage.VendorFavour(vendorfavour);
            //validate
            vendorpage.Bankdetailsverification();
            //lblValidationstatus  
            vendorpage.wait();
            Thread.Sleep(1000);
            //save button
            vendorpage.Banksavebutton();
            //bankdetail grid close 
            vendorpage.Bankdetailgridclose();           
            //vendordetailsconfirm
            vendorpage.VendorConfirm();            
            vendorpage.Alert();
        }
    }
}
