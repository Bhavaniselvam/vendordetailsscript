﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlicUnoBooksApplication.Base
{
    public class BaseTest
    {
        public static IWebDriver driver;


        [SetUp]
        public void SetUp()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            driver = new ChromeDriver(options);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            //driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);

        }

        [TearDown]
        public void TearDown()
        {
           // driver?.Quit();
        }

    }
}
