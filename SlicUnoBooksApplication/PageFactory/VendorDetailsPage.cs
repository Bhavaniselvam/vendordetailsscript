﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
namespace SlicUnoBooksApplication.PageFactory
{
    public class VendorDetailsPage
    {

        private By WindowmenuLoc = By.Id("btnwindowsmenu");
        private By vendoropenLoc = By.Id("subMenu40110409");
        private By VendornameLoc = By.Id("txtVendorDescr");
        private By NameasperpanLoc = By.Id("TxtVendorName");
        private By AddressbuttonLoc = By.Id("btnAddress");
        private By Address1Loc = By.Id("txtaddress1");
        private By PincodebuttonLoc = By.Id("btnPincode");
        private By VendorMainframeLoc = By.XPath("//iframe[contains(@src,'../GA/Masters/GGac_M_VendorDetails.aspx?menuID=40110409&SubModuleId=401&Unit=FinRg&AccPeriodDescr=JUN2020&AccPeriodPk=158&AccessRights=Y,Y,Y,N,N,N,Y,N,Y,Y,N,N,N,0&Processflowid=0&hdnUserid=ADMIN&ScreenModeAccess=1,1,1,0,0,1,1,1,0,0,0,0')]");
        private By AddresssearchdropdownLoc = By.Id("searchddl");
        private By AddresssearchtextboxLoc = By.Id("txtsearch");
        private By PicklistdataselectionLoc = By.XPath("//div[contains(@onclick,'select1(this);')]");
        private By AddressokbuttonLoc = By.Id("btnOK");
        private By ContactpersonLoc = By.Id("txtContactPerson");
        private By ResstatusLoc = By.Id("btnResSta");
        private By ResstatussearchLoc = By.Id("txtsearch");
        private By ResstatusselectionLoc = By.XPath("//div[contains(@onclick,'select1(this);')]");
        private By PannumberLoc = By.Id("txtpanno");
        private By TDSselectionLoc = By.Id("cboTdsDiv");
        private By OptionselectionLoc = By.Id("cboAstOption");
        private By MobilenumberLoc = By.Id("txtMobileNo");
        private By BankdetailsdropdownLoc = By.Id("btn_BankDetails");
        private By BankdetailsAddLoc = By.Id("grdDet_btnAdd");
        private By BanknamepicklistLoc = By.Id("btnBankShortName1");
        private By BanksearchtextLoc = By.Id("txtsearch");
        private By BanknameselectionLoc = By.XPath("//div[contains(@onclick,'select1(this);')]");
        // private By BanknameselectionLoc = By.XPath("//div[contains(text(),' HDFC BANK')]");
        private By BankbranchpicklistLoc = By.Id("btnBankBranch1");
        private By BankbranchsearchtextLoc = By.Id("txtsearch");
        private By BankbranchselectionLoc = By.XPath("//div[contains(@onclick,'select1(this);')]");
        //private By BankbranchselectionLoc = By.XPath("//div[contains(text(),' CHENNAI MYLAPORE')]");
        private By BankaccountnumberLoc=  By.Id("lblBankAccountNumber1");
        private By VendorfavourLoc = By.Id("txtBenfName1");
        private By BankdetailsverificationLoc = By.Id("btnBenefValidator");
        private By waitLoc = By.Id("lblValidationstatus");
        private By BanksavebuttonLoc = By.Id("grdDet_btnSave");
        private By BankdetailsgridcloseLoc = By.Id("btn_BankDetails");
        private By VendorconfirmLoc = By.Id("PageHeader_btnConfirm");




        private IWebDriver driver;
        public VendorDetailsPage(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void vendormainframe()
        {
            IWebElement Vendormainframe = driver.FindElement(VendorMainframeLoc);

            driver.SwitchTo().Frame(Vendormainframe);
        }

        public void Windowmenu()
        {

            driver.FindElement(WindowmenuLoc).Click();
        }
        public void Vendorscreenopen()
        {
            driver.FindElement(vendoropenLoc).Click();
        }
        public void Vendorname(string vendorname)
        {
            driver.FindElement(VendornameLoc).SendKeys(vendorname);
        }
        public void Nameasperpan(string nameasperpan)
        {
            driver.FindElement(NameasperpanLoc).SendKeys(nameasperpan);
        }
        public void Addressbutton()
        {
            driver.FindElement(AddressbuttonLoc).Click();
        }
        public void Address1(string Address1)
        {
            driver.FindElement(Address1Loc).SendKeys(Address1);
        }
        public void Pincodebutton()
        {
            driver.FindElement(PincodebuttonLoc).Click();
        }
        public void Addressssearchdropdown(string searchele)
        {
            IWebElement AreaEle = driver.FindElement(AddresssearchdropdownLoc);
            SelectElement selectarea = new SelectElement(AreaEle);
            selectarea.SelectByText(searchele);
        }
        public void Addressssearchtextbox(string searchtext)
        {
            driver.FindElement(AddresssearchtextboxLoc).SendKeys(searchtext);
        }
        public void Addressselection()
        {
            driver.FindElement(PicklistdataselectionLoc).Click();
        }
        public void Addressokbutton()
        {
            driver.FindElement(AddressokbuttonLoc).Click();
        }
        public void Contactperson(string contactperson)
        {
            driver.FindElement(ContactpersonLoc).SendKeys(contactperson);
        }
        public void resstatus()
        {
            driver.FindElement(ResstatusLoc).Click();
        }
        public void Resstatussearch(string resstatussearch)
        {
            driver.FindElement(ResstatussearchLoc).SendKeys(resstatussearch);
        }
        public void Resstatusselection()
        {
            driver.FindElement(ResstatusselectionLoc).Click();
        }
        public void pannumber(string pannumber)
        {
            driver.FindElement(PannumberLoc).SendKeys(pannumber);
        }
        public void TDSselection(string TDSoption)
        {
        IWebElement TDSEle = driver.FindElement(TDSselectionLoc);
        SelectElement selectTDS = new SelectElement(TDSEle);
        selectTDS.SelectByText(TDSoption);
        }
        public void optionselection(string option)
        {
            IWebElement optionEle = driver.FindElement(OptionselectionLoc);
            SelectElement selectoption = new SelectElement(optionEle);
            selectoption.SelectByText(option);
        }

        public void Mobilenumber(string mobilenumber)
        {
            driver.FindElement(MobilenumberLoc).SendKeys(mobilenumber);
        }
        public void Bankdetailsdropdown()
        {
            driver.FindElement(BankdetailsdropdownLoc).Click();
        }
        public void BankdetailsAdd()
        {
            driver.FindElement(BankdetailsAddLoc).Click();
        }
        public void Banknamepicklist()
        {
            driver.FindElement(BanknamepicklistLoc).Click();
        }
        public void Bankserchtext(string banksearch)
        {
            driver.FindElement(BanksearchtextLoc).SendKeys(banksearch);
        }
        public void Banknameselection()
        {
            driver.FindElement(BanknameselectionLoc).Click();
        }
        public void Bankbranchpicklist()
        {
            driver.FindElement(BankbranchpicklistLoc).Click();
        }
        public void Bankbranchsearchtext(string bankbranch)
        {
            driver.FindElement(BankbranchsearchtextLoc).SendKeys(bankbranch);
        }
        public void Bankbranchselection()
        {
            driver.FindElement(BankbranchselectionLoc).Click();
        }
        public void Bankaccountnumber(string accountnumber)
        {
            driver.FindElement(BankaccountnumberLoc).SendKeys(accountnumber);
        }
        public void VendorFavour(string vendorfovour)
        {
            driver.FindElement(VendorfavourLoc).SendKeys(vendorfovour);
        }
        public void Bankdetailsverification()
        {
            driver.FindElement(BankdetailsverificationLoc).Click();
        }
        public void wait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(waitLoc));
        }
        public void Banksavebutton()
        {
            driver.FindElement(BanksavebuttonLoc).Click();
        }
        public void Bankdetailgridclose()
        {
            driver.FindElement(BankdetailsgridcloseLoc).Click();
        }
        public void Alert()
        {
            WebDriverWait Alertwait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            var alert=Alertwait.Until(x => x.SwitchTo().Alert());
            var alertmessage=alert.Text;
            Console.WriteLine(alertmessage);
            alert.Accept();
            StringAssert.Contains("Vendor Short Description", alertmessage);
           
        }
        public void VendorConfirm()
        {
            driver.FindElement(VendorconfirmLoc).Click();
        }



    }
}
