﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlicUnoBooksApplication.PageFactory
{
    public class LoginPage
    {
        private By UsernameLoc = By.Id("txtUsername");
        private By PasswordLoc = By.Id("txtPassword");
        private By UnitLoc = By.Id("txtUnit");
        private By FinyearclearLoc = By.Id("txtFYear");
        private By SubmitLoc = By.Id("btnSubmit");

        private IWebDriver driver;

        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void Switchtowindow()
        {
            driver.SwitchTo().Window(driver.WindowHandles.Last());
        }
        public void Username(string username)
        {
            driver.FindElement(UsernameLoc).SendKeys(username);
        }
        public void Password(string password)
        {

            driver.FindElement(PasswordLoc).SendKeys(password);
        }
        public void unit(string unit)
        {
            driver.FindElement(UnitLoc).SendKeys(unit);
        }
        public void finyearclear()
        {
            driver.FindElement(FinyearclearLoc).Clear();
        }
        public void finyear(string year)
        {
            driver.FindElement(FinyearclearLoc).SendKeys(year);
        }
        public void submit()
        {
            driver.FindElement(SubmitLoc).Click();
        }
    }
}
